# backend-django-test

## Running the project

First you need to install docker and docker-compose. Besides that, make a clone 
of the code and open a terminal on the root directory of that project.

* Setup environment variables (linux)

```bash
source .env
```

* First you need start the postgres database

```bash
sudo docker-compose up -d database
```
* Now you need to apply the migrations on the database
```bash
sudo docker-compose run dev migrate
```
* Start the server
```bash
sudo docker-compose up dev
```

## Running tests

For run all test suite use the commmand bellow

```bash
sudo docker-compose run dev test
```

To run the statical code analysis run

## Static code analysis

```bash
sudo docker-compose run lint code/*
```

## API reference

The server have a route "/docs" that provides a **openapi** schema.
For a more "user-like" interface use the "/redoc"
