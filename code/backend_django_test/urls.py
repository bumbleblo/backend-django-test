"""backend_django_test URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.views.generic import TemplateView
from market import urls as market_urls
from rest_framework_simplejwt.views import TokenObtainPairView, TokenRefreshView
from users import urls
urlpatterns = [
    path('admin/', admin.site.urls),
    path(
        'docs/',
        TemplateView.as_view(
            template_name='openapi-schema.yml',
        ),
        name='openapi-schema'
    ),
    path(
        'redoc/',
        TemplateView.as_view(
            template_name='redoc.html',
            extra_context={'schema_url' : 'openapi-schema'}
        ),
        name='redoc'
    ),
    path('token/', TokenObtainPairView.as_view(), name='token-auth'),
    path('token/refresh/', TokenRefreshView.as_view(), name='token-refresh'),
]

urlpatterns += urls.router.urls
urlpatterns += market_urls.router.urls
