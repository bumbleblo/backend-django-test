from django.apps import AppConfig


class MarketConfig(AppConfig):
    """
    Market app configuration
    """

    name = 'market'
