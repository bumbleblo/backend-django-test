# Generated by Django 3.0.4 on 2020-03-07 23:06

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('market', '0004_auto_20200307_1703'),
    ]

    operations = [
        migrations.AddField(
            model_name='piece',
            name='amount',
            field=models.IntegerField(default=0),
        ),
    ]
