from django.db import models
from users.models import Customer


class Category(models.Model):
    """
    Represents a category of a piece
    """
    name = models.CharField(max_length=40)
    description = models.CharField(max_length=300)

class Piece(models.Model):
    """
    Represent a merchandise. That model have some
    fields for financial managment.
    """

    name = models.CharField(max_length=80)

    description = models.CharField(max_length=400)

    amount = models.IntegerField(default=0)

    seller = models.ForeignKey(
        Customer,
        on_delete=models.DO_NOTHING,
        null=True
    )

    price = models.DecimalField(
        max_digits=7,
        decimal_places=3
    )

    category = models.ForeignKey(
        Category,
        on_delete=models.SET_NULL,
        null=True
    )

class Buy(models.Model):
    """
    A purchase of a customer
    """

    buyer = models.ForeignKey(
        Customer,
        on_delete=models.SET_NULL,
        null=True,
        related_name='buyer'
    )

    item = models.ForeignKey(
        Piece,
        on_delete=models.SET_NULL,
        null=True,
        related_name='item'
    )

    seller = models.ForeignKey(
        Customer,
        on_delete=models.SET_NULL,
        null=True,
        related_name='seller'
    )

    amount = models.IntegerField(null=True)
