from market.models import Piece, Category, Buy
from rest_framework.serializers import ModelSerializer

class CategorySerializer(ModelSerializer):
    """
    Serializer of the Category model
    """

    class Meta:
        """
        Serializer MetaClass
        """
        model = Category
        fields = '__all__'

class PieceSerializer(ModelSerializer):
    """
    Serializer of the Piece model
    """

    class Meta:
        """
        Serializer MetaClass
        """
        model = Piece
        fields = '__all__'

class BuySerializer(ModelSerializer):
    """
    Serializer of the Buy model
    """
    class Meta:
        """
        Serializer MetaClass
        """
        model = Buy
        fields = '__all__'
