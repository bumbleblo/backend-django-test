from market.models import Piece
from market.views import CustomerTransactionsView
from rest_framework.test import APITestCase, force_authenticate, APIRequestFactory
from users.models import Customer


class TransactionsTestCase(APITestCase):
    """
    Testing transaction actions
    """


    def setUp(self):
        """
        Create models customer and piece
        """

        # Create factory
        self.factory = APIRequestFactory()

        # Inserting a user on database
        self.user_data = {
            'username' : 'test',
            'password' : 'test',
            'cpf_cnpj' : '123.123.123-66',
        }

        self.customer = Customer(
            username=self.user_data['username'],
            password=self.user_data['password'],
            cpf_cnpj=self.user_data['cpf_cnpj'],
        )


        self.customer.save()
        self.customer.groups.set([1, 2])
        self.customer.refresh_from_db()

        # Create a piece to buy
        self.piece = Piece(
            name='jorge',
            description='resume',
            amount=40,
            price=13.90
        )

        self.piece.save()

        # Create view
        self.view = CustomerTransactionsView.as_view(
            {
                'post' : 'create',
                'get' : 'list'
            }
        )

    def test_buy_something(self):
        """
        Buy a piece
        """

        transaction_data = {
            "buyer" : self.customer.pk,
            "seller" : self.customer.pk,
            "item" : self.piece.pk,
            "amount" : 20
        }

        request = self.factory.post(
            '/transactions/',
            transaction_data
        )

        piece_amount = self.piece.amount

        force_authenticate(request, self.customer)
        response = self.view(request)

        piece_amount_after = Piece.objects.get(pk=self.piece.pk).amount

        self.assertEqual(response.status_code, 200)
        self.assertLess(piece_amount_after, piece_amount)
