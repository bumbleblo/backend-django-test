from rest_framework.routers import DefaultRouter
from market.views import PieceView, CategoryView, CustomerTransactionsView

router = DefaultRouter()
router.register(r'pieces', PieceView, basename='piece')
router.register(r'category', CategoryView, basename='category')
router.register(r'transactions', CustomerTransactionsView, basename='transactions')
