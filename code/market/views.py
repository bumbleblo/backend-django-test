from market.models import Piece, Category, Buy
from market.serializers import BuySerializer
from market.serializers import PieceSerializer, CategorySerializer
from rest_framework import viewsets, mixins
from rest_framework.response import Response
from rest_framework.viewsets import GenericViewSet
from users.permissions import IsSeller, IsClient

class PieceView(
        GenericViewSet,
        mixins.ListModelMixin,
        mixins.CreateModelMixin,
        mixins.UpdateModelMixin,
        mixins.RetrieveModelMixin,
        mixins.DestroyModelMixin
    ):
    """
    Piece CRUD viewset
    """

    permission_classes = [IsSeller]
    serializer_class = PieceSerializer
    queryset = Piece.objects.all()

    def list(self, request):
        """
        Get a list of all pieces by that user
        """

        user = request.user

        query = Piece.objects.filter(seller=user.pk)

        serialized_query = PieceSerializer(query, many=True)

        return Response(serialized_query.data)


    def create(self, request):
        """
        Custom create method
        """

        data = request.data

        data['seller'] = request.user.pk

        piece = PieceSerializer(data=data)

        message = ''
        if piece.is_valid():
            piece.save()
            message = data
        else:
            message = piece.errors

        return Response(message)

# TODO(felipe) maybe IsAdmin permissons is better for that
class CategoryView(
        GenericViewSet,
        mixins.ListModelMixin,
        mixins.CreateModelMixin,
        mixins.RetrieveModelMixin
    ):
    """
    CategoryViewSet
    actions: list, create, retrieve
    """

    serializer_class = CategorySerializer
    queryset = Category.objects.all()

class CustomerTransactionsView(
        viewsets.GenericViewSet,
        mixins.CreateModelMixin,
        mixins.ListModelMixin
    ):
    """
    CustomerTransactions Viewset
    actions: create, list
    """

    permission_classes = [IsClient]
    serializer_class = BuySerializer

    def create(self, request):
        """
        Buy something
        """

        buy_object = BuySerializer(data=request.data)
        message = ''

        if buy_object.is_valid():

            piece_pk = request.data['item']
            piece = Piece.objects.get(pk=piece_pk)

            amount_buyed = buy_object.validated_data['amount']
            if piece.amount >= amount_buyed:
                piece.amount = piece.amount - amount_buyed

                piece.save()
                buy_object.save()
                message = "buyed"
            else:
                return Response(
                    "The amount buyed it's greater that we have on stock",
                    400
                )

        else:
            message = buy_object.errors

        return Response(message)

    def list(self, request):
        """
        Retrieve all transactions by user
        """

        user_pk = request.user.pk

        query = Buy.objects.filter(buyer=user_pk)

        serialized_query = BuySerializer(
            query,
            many=True
        )

        return Response(serialized_query.data)
