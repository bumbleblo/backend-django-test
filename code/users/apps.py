from django.apps import AppConfig


class UsersConfig(AppConfig):
    """
    App configuration for users app
    """
    name = 'users'
