from users.models import Customer
from django.contrib.auth.models import Permission, Group
from django.contrib.contenttypes.models import ContentType

client_group, _ = Group.objects.get_or_create(name='client')
seller_group, _ = Group.objects.get_or_create(name='seller')

contenttype = ContentType.objects.get_for_model(Customer)

print(contenttype)

client_permission = Permission.objects.create(
    name='Client',
    codename='client',
    content_type=contenttype
)

seller_permission = Permission.objects.create(
    name='Seller',
    codename='seller',
    content_type=contenttype
)

client_group.permissions.add(client_permission)
seller_group.permissions.add(seller_permission)
