from django.db import models
from django.contrib.auth.models import AbstractUser

class User(AbstractUser):
    #pylint: disable = line-too-long, unnecessary-pass
    """
    It's a good pratice according to the
    [official documentation](https://docs.djangoproject.com/en/3.0/topics/auth/customizing/#using-a-custom-user-model-when-starting-a-project)
    """
    pass

class Customer(User):
    """
    User user class
    """

    # TODO(bumbleblo) Write validators for this
    cpf_cnpj = models.CharField(max_length=40)
