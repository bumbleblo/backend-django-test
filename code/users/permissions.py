from rest_framework.permissions import BasePermission
from users.models import Customer
from django.contrib.auth.models import Group

class IsSeller(BasePermission):
    """
    That permission just give permission to
    users that are in Seller gruop.
    """

    def has_permission(self, request, view):
        """
        Custom permission based on Seller group.
        """

        seller_group = Group.objects.get(name='client')

        try:
            user = Customer.objects.get(pk=request.user.pk)
        except:
            return False

        if seller_group in user.groups.all():
            return True

        return False

class IsClient(BasePermission):
    """
    This permission allows Customer that are in
    client group.
    """

    def has_permission(self, request, view):
        """
        Custom permission based on seller group
        """

        seller_group = Group.objects.get(name='seller')

        try:
            user = Customer.objects.get(pk=request.user.pk)
        except:
            return False

        if seller_group in user.groups.all():
            return True

        return False
