from rest_framework.serializers import ModelSerializer
from users.models import Customer
from django.contrib.auth.hashers import make_password

class CustomerSerializer(ModelSerializer):
    """
    Customer Serializer
    """

    class Meta:
        """
        Filter Customer fields
        """

        model = Customer
        exclude = [
            'is_superuser',
            'is_staff',
            'is_active',
            'user_permissions'
        ]

    def validate_password(self, value):
        """
        https://stackoverflow.com/questions/55906891/django-drf-simple-jwt-authenticationdetail-no-active-account-found-with-the
        """

        return make_password(value)
