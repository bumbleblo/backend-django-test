import logging

from rest_framework.test import APITestCase
from users.serializers import CustomerSerializer

logger = logging.getLogger(__name__)

class TestJWTAuthentication(APITestCase):
    """
    Testing JWT permissions, token generator and
    refresh
    """

    def setUp(self):
        """
        Creating some test objects
        """

        self.user_data = {
            'username' : 'test',
            'email' : 'teste@test.com',
            'password' : 'test',
            'cpf_cnpj' : '111.111.111-66'
        }

        serializer = CustomerSerializer(data=self.user_data)

        if serializer.is_valid():
            serializer.save()
        else:
            print(serializer.errors)

    def test_trying_without_authentication(self):
        """
        Trying to authenticate in all endpoints
        """

        urls = ['/users/']

        logger.debug('List of endpoints: {urls}')

        responses = []
        for url in urls:
            response = self.client.get(url, self.user_data)
            responses.append(response)


        for response in responses:
            self.assertEqual(response.status_code, 401)

    def test_get_token(self):
        """
        Getting token data
        """
        response = self.client.post(
            '/token/',
            {
                'username' : 'test',
                'password' : 'test'
            }
        )

        self.assertEqual(response.status_code, 200)
