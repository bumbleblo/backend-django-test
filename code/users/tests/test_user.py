from rest_framework.test import APITestCase, force_authenticate, APIRequestFactory
from users.models import Customer
from users.serializers import CustomerSerializer
from users.views import CustomerView


class TestCustomerView(APITestCase):
    """
    Testing actions of UserViewSet
    """

    def setUp(self):
        """
        Insert Customers on database
        """

        # Create factory
        self.factory = APIRequestFactory()

        self.user_data = {
            'username' : 'test',
            'password' : 'test',
            'cpf_cnpj' : '111.111.112-66',
            'groups': [1, 2]
        }

        self.customer = CustomerSerializer(data=self.user_data)

        if self.customer.is_valid():
            self.customer.save()
        else:
            raise Exception("Customer not created on test")

    def test_create_a_minimum_user(self):
        """
        Create a user with minimum fields
        """
        user_data = {
            'username' : 'test',
            'password' : 'test',
            'cpf_cnpj' : '111.111.111-66'
        }

        response = self.client.post('/users/', user_data)

        query = Customer.objects.all()

        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(query), 1)

    def test_create_user_with_groups(self):
        """
        Test if you can create groups
        """

        response = self.client.post(
            '/users/',
            self.user_data
        )

        self.assertEqual(
            response.status_code,
            200
        )

    def test_get_user_information(self):
        """
        Get the information about a user
        """

        customer_model_obj = Customer.objects.get(username='test')
        request = self.factory.get(f'/users/{customer_model_obj.pk}/')

        view = CustomerView.as_view(
            {'get' : 'retrieve'}
        )

        force_authenticate(request, customer_model_obj)

        response = view(request, pk=customer_model_obj.pk)

        self.assertEqual(response.status_code, 200)

    def test_destroy_user(self):
        """
        Destroy a user on database
        """

        customer_model_obj = Customer.objects.get(username='test')
        request = self.factory.delete(f'/users/{customer_model_obj.pk}/')
        force_authenticate(request, customer_model_obj)

        view = CustomerView.as_view(
            {'delete' : 'destroy'}
        )

        response = view(request, pk=customer_model_obj.pk)

        self.assertEqual(response.status_code, 200)
