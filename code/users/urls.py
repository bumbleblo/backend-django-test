from rest_framework.routers import DefaultRouter
from users.views import CustomerView

router = DefaultRouter()
router.register(r'users', CustomerView, basename='user')
