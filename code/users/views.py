from rest_framework import mixins
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response
from rest_framework.viewsets import GenericViewSet
from users.models import Customer
from users.serializers import CustomerSerializer


class CustomerView(
        GenericViewSet,
        mixins.CreateModelMixin,
        mixins.RetrieveModelMixin,
        mixins.UpdateModelMixin,
        mixins.DestroyModelMixin
    ):
    """
    CRUD of the Customer user.
    You don't need to be autenticated to use the 'create' method,
    All the others methods use the JWT authentication, see more in 'token'
    """

    # TODO(felipe) verify if the user authenticated is the same that on the pk

    serializer_class = CustomerSerializer
    queryset = Customer.objects.filter(is_active=True)

    def create(self, request):
        """
        Create a user
        """

        customer = CustomerSerializer(data=request.data)

        if customer.is_valid():
            customer.save()
            message = f'The user was created'
        else:
            message = customer.errors


        return Response(message)

    def destroy(self, request, pk=None):
        """
        Deactivate a user. You don't want do delete
        a user, if this is done the purchases maded will be
        not related with a buyer.
        """

        try:
            customer = Customer.objects.get(pk=pk)
        except Exception:
            return Response({"errors" : "user not found"}, 404)
        customer_username = customer.username

        message = ''

        if customer.is_active == True:

            customer.is_active = False
            customer.save()

            message = f'The user {customer_username} has been removed'

        else:
            message = f'This user is already removed'


        return Response(message)

    def get_permissions(self):
        """
        Allow permissions for all action except the create action
        """

        if self.action == 'create':
            permission_classes = [AllowAny]
        else:
            permission_classes = [IsAuthenticated]

        return [permission() for permission in permission_classes]
